#!/usr/bin/env python3
from setuptools import setup
import versioneer

PROJECT = 'fw1884_osc_ardour'

setup(name=PROJECT,
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      author='Scott Bahling',
      author_email='sbahling@mudgum.net',
      packages=[PROJECT],
      entry_points = {
          'console_scripts': ['fw1884-osc-ardour=fw1884_osc_ardour.cli:main'],
          },
      )
