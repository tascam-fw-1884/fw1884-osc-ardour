OSC interface between Ardour and the Tascam FW-1884 control surface
===================================================================

This is an Open Sound Control based bridge between the OSC interface
of Ardour and the OSC interface of the Tascam FW-1884 control surface
which is provided by the `tascam-fw-console` daemon.


