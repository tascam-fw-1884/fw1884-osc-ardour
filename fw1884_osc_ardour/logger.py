import logging

class CustomFormatter(logging.Formatter):

    colors = {'black': '\x1b[30m',
              'red': '\x1b[31m',
              'green': '\x1b[32m',
              'yellow': '\x1b[33m',
              'blue': '\x1b[34m',
              'magenta': '\x1b[35m',
              'cyan': '\x1b[36m',
              'white': '\x1b[37m',
              'dgray': '\x1b[90m',
              'lred': '\x1b[91m',
              'lgreen': '\x1b[92m',
              'lyellow': '\x1b[93m',
              'lblue': '\x1b[94m',
              'lmagenta': '\x1b[95m',
              'lcyan': '\x1b[96m',
              'lgray': '\x1b[97m',
              }

    reset = '\x1b[0m'

    log_format = '%(asctime)s - %(name)s   | %(levelname)s | %(message)s'

    def __init__(self, color, bold=False):
        self.color = self.colors.get(color, self.colors.get('white'))
        if bold:
            self.color = self.color + '\x1b[3m'
        else:
            self.color = self.color + '\x1b[21m'
        self.log_fmt = self.color + self.log_format + self.reset

    def format(self, record):  # noqa:
        formatter = logging.Formatter(self.log_fmt)
        return formatter.format(record)
