#!/usr/bin/env python3
"""
    Open Sound Control Tascam Firewire control surface to Ardour translator
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details

"""

import logging

from fw1884_osc_ardour import control_state
from fw1884_osc_ardour import fw1884
from fw1884_osc_ardour.logger import CustomFormatter


logger = logging.getLogger('ardour_server')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter(color='lblue'))

logger.addHandler(ch)
logger.propagate = False


client_logger = logging.getLogger('ardour_client')
client_logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter(color='lblue', bold=True))

client_logger.addHandler(ch)
client_logger.propagate = False

client = None
fw1884_client = None

# Listen for messages from FW1884 OSC client
server_ip = '127.0.0.1'
server_port = '9100'

# Ardour OSC server
client_ip = '127.0.0.1'
client_port = '3819'

button_pressed = {}

translation = {b'/button/transport_ffwd': '/ffwd',
               b'/button/transport_rew': '/rewind',
               b'/button/transport_stop': '/transport_stop',
               b'/button/transport_play': '/transport_play',
               b'/button/bank_left': '/bank_down',
               b'/button/bank_right': '/bank_up',
               b'/button/in': '/toggle_punch_in',
               b'/button/out': '/toggle_punch_out',
               b'/button/shortcuts/clr_solo': '/cancel_all_solos',
               b'/button/shortcuts/loop': '/loop_toggle',
               b'/button/transport_rec': '/rec_enable_toggle',
               }


async def bind(server, sock, fw1884_sock):

    logger.debug(f'{sock} {fw1884_sock}')

    async def default_handler(addr, *args):
        """Simply pass through to ardour"""
        newaddr = translation.get(addr, None)
        if newaddr is not None:
            logger.debug(f'{newaddr} {args}')
            await send_message(newaddr, args)
        else:
            logger.warn(f'Ardour Proxy Server: {addr.decode("utf-8")} {args}')

    server.default_handler = default_handler

    async def send_message(addr, values):
        if not isinstance(values, tuple) and not isinstance(values, list):
            values = (values,)
        logger.debug(f'{addr}, {values}, {server_ip}, {server_port}, {sock}')
        logger.debug(f'Send message to {client_ip} {client_port}')
        await server.send_message(addr, values,
                                  ip_address=client_ip,
                                  port=client_port,
                                  sock=fw1884_sock,
                                  encoding='utf-8'
                                  )

    @server.address('/button/computer', sock, get_address=True)
    async def set_surface(addr, *args):
        logger.warn(f'FW-1884 Server: {addr.decode("utf-8")} {args}')
        await send_message('/set_surface', (8, 159, 19, 1, 8, 11))

    @server.address('/strip/fader', sock, get_address=True)
    async def set_fader(addr, strip, pos):
        logger.info(f'{addr}, {strip}, {pos}')
        pos = pos / 1023
        await send_message(addr, (strip, pos))

    @server.address('/strip/fader_touch', sock, get_address=True)
    async def handle_fader_touch(addr, strip, state):
        logger.debug(f'handle_fader_touch {strip}, {state}')
        # await send_message('/strip/select', (strip, state))
        # await send_message('/strip/fader/touch', (strip, state))

    @server.address('/jogwheel', sock, get_address=True)
    async def handle_jogwheel(addr, delta):
        if button_pressed.get('ctrl') and button_pressed.get('shift'):
            await send_message('/jog/mode', 5)
            await send_message('/jog', delta)
        elif button_pressed.get('shtl'):
            await send_message('/jog/mode', 3)
            await send_message('/jog', delta)
        elif button_pressed.get('shift'):
            await send_message('/jog/mode', 2)
            await send_message('/jog', delta)
        elif button_pressed.get('ctrl'):
            await send_message('/jog/mode', 1)
            await send_message('/jog', delta)
        else:
            await send_message('/jog/mode', 0)
            await send_message('/jog', delta)

    @server.address('/button/encoders/*', sock, get_address=True)
    async def set_encoder_mode(addr, state):
        button = addr.split(b'/')[-1]
        logger.debug(f'encoder_mode: {button}')
        await server.send_message('/encoder_mode', (button,),
                                  ip_address=fw1884.server_ip,
                                  port=fw1884.server_port,
                                  sock=fw1884_sock,
                                  encoding='utf-8'
                                  )

    @server.address('/strip/encoder', sock, get_address=True)
    async def handle_encoder(addr, strip, delta):
        logger.info(f'{addr} {strip} {delta}')
        logger.debug(f'encoder_mode: {control_state.get("encoder_mode")}')

        async def set_trim(strip, delta):
            key = f'TRIM{strip}'
            pos = control_state.get(key, 0.0)
            pos = pos + delta * 0.5
            if pos < -20:
                pos = -20
            if pos > 20:
                pos = 20
            await send_message('/strip/trimdB', (strip, pos))

        async def set_pan(strip, delta):
            key = f'PAN{strip}'
            pos = control_state.get(key, 0.5)
            pos = pos - delta * 0.02
            if pos < 0:
                pos = 0
            if pos > 1.0:
                pos = 1.0
            logger.debug(f'/strip/pan_stereo_position {strip}, {pos}')
            await send_message('/strip/pan_stereo_position', (strip, pos))

        if control_state.get('encoder_mode') == 'PAN':
            if button_pressed.get('alt'):
                await set_trim(strip, delta)
            else:
                await set_pan(strip, delta)

    # #################################
    # Button handlers
    # #################################

    @server.address('/button/arrow_*', sock, get_address=True)
    async def handle_arrow_button(addr, state):
        if state == 0:
            return

        delta = 1.0
        if button_pressed.get('shift'):
            delta = 4.0

        logger.info(f'{addr}')
        direction = addr.split(b'/')[-1]
        if direction == 'arrow_left':
            await send_message('/jump_seconds', delta * -1.0)
        if direction == 'arrow_right':
            await send_message('/jump_seconds', delta)
        if direction == 'arrow_down':
            await send_message('/jump_bars', delta * -1.0)
        if direction == 'arrow_up':
            await send_message('/jump_bars', delta)

    @server.address('/button/locate_*', sock, get_address=True)
    async def handle_locate_button(addr, state):
        if state == 0:
            return

        direction = addr.split(b'/')[-1]

        if direction == b'locate_left':
            if button_pressed.get('shift'):
                cmd = '/goto_start'
            else:
                cmd = '/prev_marker'
        else:
            if button_pressed.get('shift'):
                cmd = '/goto_end'
            else:
                cmd = '/next_marker'

        await send_message(cmd, 1)

    @server.address('/button/transport*', sock, get_address=True)
    @server.address('/button/bank*', sock, get_address=True)
    @server.address('/button/{in,out}', sock, get_address=True)
    async def handle_button(addr, state):
        newaddr = translation.get(addr, None)
        if newaddr is not None:
            logger.debug(f'{newaddr} {state}')
            await send_message(newaddr, state)
        else:
            logger.info(f'Button Pressed {addr} {state}')
            button = addr.split(b'/')[-1]
            button_pressed[button] = bool(state)

    @server.address('/button/shortcuts/marker', sock, get_address=True)
    async def handle_marker_button(addr, state):
        if state == 0:
            return

        if button_pressed.get('shift'):
            cmd = '/remove_marker'
        else:
            cmd = '/add_marker'

        await send_message(cmd, 1)

    @server.address('/button/strip/{solo,mute,sel}', sock, get_address=True)
    async def handle_strip_button(addr, strip, state):
        async def handle_sel_button(strip):
            if button_pressed.get('rec_enable'):
                if control_state.get(f'recenable{strip}'):
                    await send_message('/strip/recenable', (strip, 0))
                else:
                    await send_message('/strip/recenable', (strip, 1))
            else:
                if control_state.get(f'sel{strip}'):
                    await send_message('/strip/select', (strip, 0))
                else:
                    await send_message('/strip/select', (strip, 1))

        logger.info('handle_strip_button')
        if not state:
            return

        button = addr.split(b'/')[-1]

        if button == 'sel':
            await handle_sel_button(strip)
            return

        key = f'{button}{strip}'
        current = control_state.get(key)
        if current:
            await send_message('/strip/%s' % (button.decode('utf-8')), (strip, 0))
        else:
            await send_message('/strip/%s' % (button.decode('utf-8')), (strip, 1))
