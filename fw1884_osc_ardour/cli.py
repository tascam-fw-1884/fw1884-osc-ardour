#!/usr/bin/env python3
"""
    Open Sound Control Tascam Firewire control surface to Ardour translator
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018-2022 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details

Usage:
    tascam-ardour-osc [options]

Options:
    -h --help                 Show this screen.
    -l --list                 List connected Tascam FW console units
    --fw1884-client=ADDR
        Address:port s for communicating to FW1884 [default: 127.0.0.1:8100]
    --fw1884-server=ADDR
        Address:port for messages from Ardour [default: 127.0.0.1:8000]
    --ardour-client=ADDR
        Address:port for communicating to Ardour [default: 127.0.0.1:3819]
    --ardour-server=ADDR
        Address:port to messages from FW1884 [default: 127.0.0.1:9100]
"""

import ipaddress
from collections import namedtuple

from docopt import docopt

from oscpy.server.trio_server import OSCTrioServer

import trio

from fw1884_osc_ardour import ardour
from fw1884_osc_ardour import fw1884


DEFAULT_PORTS = {'fw1884-client': fw1884.client_port,
                 'fw1884-server': fw1884.server_port,
                 'ardour-client': ardour.client_port,
                 'ardour-server': ardour.server_port,
                 }

Address = namedtuple('Address', 'ip, port')


def _is_valid_ip(ip):
    try:
        ipaddress.ip_address(ip)
    except Exception:
        return False

    return True


def parse_addr(service, args):
    addr = args['--{}'.format(service)]
    if ':' in addr:
        ip, port = addr.split(':', 1)
    else:
        ip = addr
        port = DEFAULT_PORTS.get(service)

    try:
        port = int(port)
    except Exception:
        raise('In:qvalid Port: {}'.format(port))

    if not _is_valid_ip(ip):
        raise('Invalid IP {}'.format(ip))

    return Address(ip, port)


async def app():
    global server

    server = OSCTrioServer(encoding='utf8', advanced_matching=True)

    # Listening for messages from FW1884 OSC Client
    ardour_sock = await server.listen(ardour.server_ip, ardour.server_port)
    # Listening for messages from Ardour OSC client
    fw1884_sock = await server.listen(fw1884.server_ip, fw1884.server_port)

    await ardour.bind(server, ardour_sock, fw1884_sock)
    await fw1884.bind(server, fw1884_sock, ardour_sock)

    await server.process()


def main():

    args = docopt(__doc__, version='0.1')

    # fw1884 module receives messages from the Ardour OSC client
    # and dispatches messages to the fw1884 OSC server
    addr = parse_addr('fw1884-server', args)
    fw1884.server_ip = addr.ip
    fw1884.server_port = addr.port

    # ardour module receives messages from the FW1884 OSC client
    # and dispatches messages to Ardour OSC server
    addr = parse_addr('ardour-server', args)
    ardour.server_ip = addr.ip
    ardour.server_port = addr.port

    trio.run(app)


if __name__ == '__main__':
    main()
