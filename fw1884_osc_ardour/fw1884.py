#!/usr/bin/env python3
"""
    Open Sound Control Tascam Firewire control surface to Ardour translator
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details

"""
import logging

from fw1884_osc_ardour import control_state
from fw1884_osc_ardour.logger import CustomFormatter


logger = logging.getLogger('fw1884_server')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter(color='lred'))

logger.addHandler(ch)
logger.propagate = False

client_logger = logging.getLogger('fw1884_client')
client_logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter(color='lred', bold=True))

client_logger.addHandler(ch)
client_logger.propagate = False

client = None
ardour_client = None

# Listen for messages from Ardour OSC client
server_ip = '127.0.0.1'
server_port = '8000'

# Tascam FW1884 OSC server
client_ip = '127.0.0.1'
client_port = '8100'


def strip_name(strip):
    return control_state.get(f'strip_name{strip}')


def strip_in_reset(strip):
    """When switching banks, Ardour first sets all control values of the
    strips to 0 and the name of the strip to ' '. Reacting on these commands
    is useless and just causes the faders on the FW-1884 to jump. So we ignore
    commands while the strip name == ' '.
    This function is a helper for checking if the strip is currently in
    'reset' mode.
    """
    if strip_name(strip) == ' ':
        logger.info('******** strip reset ******')
        return True

    return False


async def bind(server, sock, ardour_sock):

    logger.debug(f'{sock} {ardour_sock}')
    logger.debug(f'{server_port} {client_port}')

    async def send_message(addr, values):
        if not isinstance(values, tuple) and not isinstance(values, list):
            values = (values,)
        logger.debug(f'{addr}, {values}, {server_ip}, {server_port}, {sock}')
        await server.send_message(addr, values,
                                  ip_address=client_ip,
                                  port=client_port,
                                  sock=ardour_sock,
                                  encoding='utf-8'
                                  )

    @server.address('/encoder_mode', sock, get_address=True)
    async def set_encoder_mode(addr, mode):
        mode = mode.upper()
        await send_message('/encoder_mode', mode)
        control_state['encoder_mode'] = mode
        logger.debug(f'encoder_mode: {mode}')

    @server.address('/strip/fader', sock, get_address=True)
    async def set_fader(addr, strip, pos):
        if strip_in_reset(strip):
            pass
        pos = pos * 1023
        logger.info(f'Set Fader: {strip}:{strip_name(strip)}, {pos}')
        await send_message(addr, (strip, pos))

    @server.address('/strip/pan_stereo_position', sock, get_address=True)
    async def set_pan(addr, strip, pos):
        if strip_in_reset(strip):
            return
        key = f'PAN{strip}'
        control_state[key] = pos

    @server.address('/strip/trimdB', sock, get_address=True)
    async def set_trim(addr, strip, pos):
        if strip_in_reset(strip):
            return
        key = f'TRIM{strip}'
        control_state[key] = pos

    @server.address('/strip/{solo,mute,sel}', sock, get_address=True)
    async def handle_strip_button(addr, strip, state):
        if strip_in_reset(strip):
            return
        button = addr.split(b'/')[-1]
        key = f'{button}{strip}'
        control_state[key] = state
        await send_message(addr.decode('utf-8'), (strip, state))

    @server.address('/strip/recenable', sock, get_address=True)
    async def set_strip_recenable(addr, strip, state):
        if strip_in_reset(strip):
            return
        key = f'recenable{strip}'
        control_state[key] = bool(state)
        await send_message(addr, (strip, state))

    @server.address('/strip/name', sock, get_address=True)
    async def set_strip_name(addr, strip, name):
        key = f'strip_name{strip}'
        oldname = control_state.get(key, 'None')
        control_state[key] = name
        logger.debug(f'strip {strip} name: {oldname} -> {name}')

    @server.address('/{ffwd}{rewind}', sock, get_address=True)
    async def ff_rew(addr, *args):
        await send_message(addr, *args)

    @server.address('/transport_*', sock, get_address=True)
    async def handle_transport_buttons(addr, *args):
        await send_message(addr, *args)

    @server.address('/rec_enable_toggle', sock, get_address=True)
    async def handle_rec_enable_toggle(addr, *args):
        await send_message(addr, *args)
